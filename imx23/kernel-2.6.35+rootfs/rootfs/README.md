# Instalación del Sistema de Ficheros

Éste sistema de ficheros debe ser instalado en una partición de la SD con formato **ext3** para
que sea localizada por el kernel 2.6.35.

Para instalar haga algo similar al siguiente ejemplo:

```bash
# comando:
lsblk

# salida del comando
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
loop0    7:0    0  24.7M  1 loop /snap/snapd/6240
loop1    7:1    0  54.7M  1 loop /snap/core18/1668
loop2    7:2    0  24.7M  1 loop /snap/snapd/6434
loop3    7:3    0 259.4M  1 loop /snap/acestreamplayer/10
sda      8:0    0 465.8G  0 disk 
├─sda1   8:1    0 102.5G  0 part /
├─sda2   8:2    0 362.4G  0 part /home
├─sda3   8:3    0     1K  0 part 
└─sda5   8:5    0   959M  0 part [SWAP]
sdb      8:16   1   1.9G  0 disk 
├─sdb1   8:17   1   100M  0 part 
└─sdb2   8:18   1   1.8G  0 part /media/johnny/my_sd
sr0     11:0    1  1024M  0 rom
```
Como en la instalación del bootloader y kernel debe identificar el nombre con el que se identifica la partición en la SD.
En éste caso se requiere la partición 2; en el ejemplo la partición 2 es la llamada **sdb2**, la cual está montada en
**/media/johnny/my_sd/** (debe montar la partición de manera manual si su sistema no lo hace automáticamente).

Para instalar el rootfs.tar ejecute algo similar a ésto:

```bash
sudo tar xvf rootfs.tar -C /media/johnny/my_sd/ && sync
```

Tenga en cuenta que su punto de montaje será distinto al que yo planteo en el ejemplo.

Johnny Cubides
