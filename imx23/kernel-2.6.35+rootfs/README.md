# Instalación del sistema

En cada uno de los directorios (`rootfs` y `uboot+kernel2.6.35`)
encontrará un README.md el cual le dará las instrucciones para
instalar ambos elementos.

Es condición instalar en la micro SD el software que encuentra en
ambas carpetas (en este caso no importa el orden).

