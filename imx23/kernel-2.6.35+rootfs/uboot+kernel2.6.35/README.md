# Instalación de Bootloader y Kernel

Inserte la tarjeta micro SD e identifique la tarjeta en su equipo, ejemplo:

```bash
# comando:
lsblk

# salida del comando
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
loop0    7:0    0  24.7M  1 loop /snap/snapd/6240
loop1    7:1    0  54.7M  1 loop /snap/core18/1668
loop2    7:2    0  24.7M  1 loop /snap/snapd/6434
loop3    7:3    0 259.4M  1 loop /snap/acestreamplayer/10
sda      8:0    0 465.8G  0 disk 
├─sda1   8:1    0 102.5G  0 part /
├─sda2   8:2    0 362.4G  0 part /home
├─sda3   8:3    0     1K  0 part 
└─sda5   8:5    0   959M  0 part [SWAP]
sdb      8:16   1   1.9G  0 disk 
├─sdb1   8:17   1   100M  0 part 
└─sdb2   8:18   1   1.8G  0 part /media/johnny/my_sd
sr0     11:0    1  1024M  0 rom
```
En mi caso la partición 1 de la  tarjeta ha sido reconocida como **sdb1**, en otros casos puede tener el nombre **mmcblk0p1**, identifique el suyo
y remplace en el siguiente comando:

```bash
sudo unzip -p sd_mmc_linux.img.zip | sudo dd of=/dev/sdb1 conv=fsync status=progress
```

