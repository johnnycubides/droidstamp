# Selección de Kernel y Sistema de Ficheros a Instalar en el DroidStamp

Seleccione un kernel+rootfs para instalar en su micro SD.

## kernel-2.6.35+rootfs

### Características del Kernel

* Kernel linux 2.6.35
* WiFi en modo Access Point

### Características del Systema de Ficheros

* CivetWeb como servidor HTTP, (montar los archivos en /var/www)
* PHP 5.x Para desarrollar aplicaciones al lado del servidor (REST API)
* OpenOcd para programar el STM32F4x
* Interprete Python con capacidad serial para recibir datos del STM32F4x

