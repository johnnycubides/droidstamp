# DroidStamp

![droidstamp](./droidstamp.png)

Placa de desarrollo que cuenta con un microprocesador imx23 y un microcontrolador stm32f405.

Puedes construir con el un servicio basado en Linux que es accesible por WiFi, hacer tareas de
software y reportarlas al usuario final, estas tareas se pueden realizar a partir de un sistema
operativo de tiempo real o a través de un interprete como micropython.

## Instrucciones

### 1. Seleccionar una localización en su PC y clonar el repositorio:

```bash
git clone -b master --single-branch https://gitlab.com/johnnycubides/droidstamp.git
```

### 2. Crear formato para micro SD

Entrar al directorio `createSD` y seguir las instrucciones del README.md

### 3. Seleccionar sistema a ejecutar en el droidstamp por imx23

Aquí se debe pensar cuál es la versión del sistema a usar; los sistemas
cubiertos y/o documentados, estarán en la lista.

Lista:

* `kernel-2.6.35+rootfs`

Seleccionado el sistema a ejecutar entre al directorio `ìmx23` y busque el directorio que 
representa el sistema, revise el respectivo README.md del directorio y siga
las instrucciones de instalación.

### 4. Construir y enviar firmware para STM32F405

Entrar al directorio y ...

## Características

### Hardware

* Imx233 processor
* 64 MB RAM
* Microcontrolador st32f405

### Software

* Linux Embebido
* ChibiOS

Mantenido por

Johnny Cubides

jgcubidesc@gmail.com
