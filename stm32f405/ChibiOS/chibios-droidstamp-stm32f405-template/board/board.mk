# List of all the board related files.
# BOARDSRC = $(CHIBIOS)/os/hal/boards/ST_STM32F4_DISCOVERY/board.c
BOARDSRC = ./board/board.c

# Required include directories
# BOARDINC = $(CHIBIOS)/os/hal/boards/ST_STM32F4_DISCOVERY
BOARDINC = ./board

# Shared variables
ALLCSRC += $(BOARDSRC)
ALLINC  += $(BOARDINC)
