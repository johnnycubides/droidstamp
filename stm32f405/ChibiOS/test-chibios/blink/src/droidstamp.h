/*
 * =====================================================================================
 *
 *       Filename:  droidstamp.h
 *
 *    Description:  Configuración adicional
 *
 *        Version:  1.0
 *        Created:  02/22/2020 03:47:13 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

/* Agregar las definiciones de pines aquí */
#define PORTLED GPIOD
#define USERLED GPIOD_PIN2

