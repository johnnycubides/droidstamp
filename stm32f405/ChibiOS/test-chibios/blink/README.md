# Uso de CHIBIOS

[Documentación ChibiOS](http://chibiforge.org/doc/19.1/full_rm/)

## Observaciones

... POR TERMINAR ...

## Compilación y envió de firmware

```bash
make
make s2st
```

## Programación del STM en el droidstamp

1. `ssh root@192.168.15.1`
2. `cd /home/stm32/`
3. `make flash`

Nota: la contraseña de root por default es *piccolo*
