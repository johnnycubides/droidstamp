/*
    ChibiOS - Copyright (C) 2006..2018 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include "ch.h"
#include "hal.h"
#include "chprintf.h"
#include "rt_test_root.h"
#include "oslib_test_root.h"
#include "src/droidstamp.h"


SerialConfig _SD1cfg;
SerialConfig *SD1cfg = &_SD1cfg;
BaseSequentialStream* bsp = (BaseSequentialStream*)&SD1;

/*
 * This is a periodic thread that does absolutely nothing except flashing
 * a LED.
 */
static THD_WORKING_AREA(waThread1, 128);
static THD_FUNCTION(Thread1, arg) {

  (void)arg;
  chRegSetThreadName("blinker");
  while (true) {
    /* palSetPad(GPIOD, GPIOD_LED3);       /1* Orange.  *1/ */
    palSetPad(PORTLED, USERLED);       /* Orange.  */
    chThdSleepMilliseconds(100);
    /* palClearPad(GPIOD, GPIOD_LED3);     /1* Orange.  *1/ */
    palClearPad(PORTLED, USERLED);     /* Orange.  */
    sdPut(&SD1, '1');
    chThdSleepMilliseconds(500);
  }
}

static THD_WORKING_AREA(waThread2, 128);
static THD_FUNCTION(Thread2, arg) {

  (void)arg;
  chRegSetThreadName("sendToUART");
  while (true){
    chThdSleepMilliseconds(1000);
    chprintf(bsp, "Hello from stm32 uart!\r\n");
    sdPut(&SD2, '0');
  }
}


/*
 * Application entry point.
 */
int main(void) {

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
  chSysInit();
 
  SD1cfg->speed = 115200;
  /* sdStart(&SD1, SD1cfg); */
  sdStart(&SD1, NULL);
  sdStart(&SD2, SD1cfg);

  palSetPadMode(PORTLED, USERLED, PAL_MODE_OUTPUT_PUSHPULL);
  palSetPadMode(GPIOA, 10, PAL_MODE_ALTERNATE(7));
  palSetPadMode(GPIOA, 9, PAL_MODE_ALTERNATE(7));

  palSetPadMode(GPIOA, 2, PAL_MODE_ALTERNATE(7));
  palSetPadMode(GPIOA, 3, PAL_MODE_ALTERNATE(7));

  /*
   * Creates the example thread.
   */
  chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);
  chThdCreateStatic(waThread2, sizeof(waThread2), NORMALPRIO, Thread2, NULL);

  /*
   * Normal main() thread activity, in this demo it does nothing except
   * sleeping in a loop and check the button state.
   */
  while (true) {
    chThdSleepMilliseconds(500);
  }
}
