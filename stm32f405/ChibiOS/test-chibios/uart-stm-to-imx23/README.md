# Uso de CHIBIOS

[Documentación ChibiOS](http://chibiforge.org/doc/19.1/full_rm/)

Algunos links revisados para desarrollar el ejemplo

https://www.playembedded.org/blog/stm32-usart-chibios-serial/

https://bitbucket.org/piccoloiflab/piccolo-iflab/src/master/ChibiOS/ChibiOS/iflab_code/stamp_code/Makefile

https://forum.arduino.cc/index.php?topic=208565.0

https://www.tutorialspoint.com/cprogramming/c_type_casting.htm

https://www.geeksforgeeks.org/gcvt-convert-float-value-string-c/

https://medium.com/@hauyang/convert-int-into-string-with-c-macro-125eeaa71600

## Observaciones

... POR TERMINAR ...

## Compilación y envió de firmware

```bash
make
make s2st
```

## Programación del STM en el droidstamp

1. `ssh root@192.168.15.1`
2. `cd /home/stm32/`
3. `make flash`

Nota: la contraseña de root por default es *piccolo*

## Ver el resultado en la consola

En la terminal remota dada por `ssh` ejecute el siguiente comando:

```bash
miniterm.py -p /dev/ttySP1 -b 115200
```
Como resultado tendrá algo similar a lo siguiente:

```bash
+
Hello from stm32 uart!
Entero: 98765, flotantes: -123.456
```

Para liberar la terminal de la conexión serial oprima `^]` (control+] al tiempo).
