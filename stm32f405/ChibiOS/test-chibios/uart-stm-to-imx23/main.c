/*
    ChibiOS - Copyright (C) 2006..2018 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include "ch.h"
#include "hal.h"
#include "chprintf.h"
#include "rt_test_root.h"
#include "oslib_test_root.h"
#include "src/droidstamp.h"
#include "src/tools.h"

/* *El puntero &SD1 hace referencia al serial de la UART 1
 * el cual en éste caso está en los pines PA10 y PA9 */
SerialConfig _SD1cfg;
SerialConfig *SD1cfg = &_SD1cfg;
/* *Pointer a secuencia para el stream en el serial SD1 */
BaseSequentialStream* bsSD1 = (BaseSequentialStream*)&SD1;

/*
 * This is a periodic thread that does absolutely nothing except flashing
 * a LED.
 */
static THD_WORKING_AREA(waThread1, 128);
static THD_FUNCTION(Thread1, arg) {
  (void)arg;
  chRegSetThreadName("blinker");
  while (true) {
    /* *LED ON */
    palSetPad(PORTLED, USERLED);       /* Orange.  */
    /* *100 mS en ON el LED */
    chThdSleepMilliseconds(100);
    /* *LED OFF */
    palClearPad(PORTLED, USERLED);     /* Orange.  */
    /* *Espera medio segundo */
    chThdSleepMilliseconds(500);
    /* Enviando el caracter + como ejemplo */
    sdPut(&SD1, '+');
  }
}

static THD_WORKING_AREA(waThread2, 128);
static THD_FUNCTION(Thread2, arg) {
  (void)arg;
  chRegSetThreadName("sendToUART");
  while (true){
    /* *Enviar un string en formato*/
    chprintf(bsSD1, "\r\nHello from stm32 uart!\r\n");
    /* *Imprimendo salidas de números formateados */
    float numberFloat = -123.456789;
    int numberInt = 98765;
    chprintf(bsSD1, "Entero: %d, flotantes: %d.%d \r\n", 
            numberInt, (int)numberFloat, getDEC(numberFloat, 3));
    /* *Esperar un segundo para enviar de nuevo el string */
    chThdSleepMilliseconds(1000);
  }
}


/*
 * Application entry point.
 */
int main(void) {

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
  chSysInit();
  
 /* *Selección del baudrate [9600, 38400, 115200] */ 
  SD1cfg->speed = 115200;
  sdStart(&SD1, SD1cfg);
  
  /* Configurar LED como salida */
  palSetPadMode(PORTLED, USERLED, PAL_MODE_OUTPUT_PUSHPULL);
 /* *Configuración de los pines de la UART para el uso del modulo serial */ 
  palSetPadMode(STM32_IMX23_UART_PORT, STM32_IMX23_UART_RX, PAL_MODE_ALTERNATE(7));
  palSetPadMode(STM32_IMX23_UART_PORT, STM32_IMX23_UART_TX, PAL_MODE_ALTERNATE(7));

  /*
   * Creates the example thread.
   */
  chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);
  chThdCreateStatic(waThread2, sizeof(waThread2), NORMALPRIO, Thread2, NULL);

  /*
   * Normal main() thread activity, in this demo it does nothing except
   * sleeping in a loop and check the button state.
   */
  while (true) {
    chThdSleepMilliseconds(500);
  }
}
