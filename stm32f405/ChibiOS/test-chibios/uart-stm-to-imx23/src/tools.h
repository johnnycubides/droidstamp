/*
 * =====================================================================================
 *
 *       Filename:  tools.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  03/22/2020 08:43:26 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include <stdlib.h>
#include "ch.h"

/**
 * @brief Esta función retorna la parte decimal de un número flotante con propósitos de impresión
 * @param número del cual se desea separar la parte flotante
 * @param Cantidad de dígitos a los cuales se desea truncar
 * @return un número entero de máximo 2^16
 **/
uint16_t getDEC(float number, uint8_t nDigits);

