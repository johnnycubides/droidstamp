/*
 * =====================================================================================
 *
 *       Filename:  tools.c
 *
 *    Description: 
 *
 *        Version:  1.0
 *        Created:  03/22/2020 08:25:44 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include "tools.h"

uint16_t getDEC(float number, uint8_t nDigits)
{
  if(number < (float)0)
    number = number * (-1);
  else if(number == (float)0)
    return 0;
  switch (nDigits) {
    case 1:
      return (uint16_t)((number - ((int)number))*10);
    case 2:
      return (uint16_t)((number - ((int)number))*100);
    case 3:
      return (uint16_t)((number - ((int)number))*1000);
    case 4:
      return (uint16_t)((number - ((int)number))*10000);
    default:
      return -1;
  }
}

