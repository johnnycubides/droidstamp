/*
 * =====================================================================================
 *
 *       Filename:  droidstamp.h
 *
 *    Description:  Configuración adicional
 *
 *        Version:  1.0
 *        Created:  02/22/2020 03:47:13 AM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

/* Agregar las definiciones de pines aquí */
#include <stdio.h> 
#define STR(x) #x
#define PORTLED GPIOD
#define USERLED GPIOD_PIN2

#define STM32_IMX23_UART_PORT GPIOA /*!< Detailed description */
#define STM32_IMX23_UART_RX 10 /*!< Pin RX entre STM32 y IMX23 */
#define STM32_IMX23_UART_TX 9 /*!< Pin TX entre STM32 y IMX23 */
#define MAX_BUFFER 15 /*!< Buffer para convertir float a string */


