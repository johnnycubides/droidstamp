#!/bin/bash -e
#						 ↑
# debug [-x -v]:[complete, abbreviated]
# Brief:	Instaldor de cross compilador para arm compatible con stm
# Author: Johnny Cubides
# e-mail: jgcubidesc@gmail.com
# date: Monday 27 April 2020
status=$?

LINK_CROSS_COMPILE=https://developer.arm.com/-/media/Files/downloads/gnu-rm/6-2017q2/gcc-arm-none-eabi-6-2017-q2-update-linux.tar.bz2?revision=2cc92fb5-3e0e-402d-9197-bdfc8224d8a5?product=GNU%20Arm%20Embedded%20Toolchain,64-bit,,Linux,6-2017-q2-update

wget -O "gcc-arm-none-eabi-6-2017-q2-update-linux.tar.bz2" $LINK_CROSS_COMPILE

mkdir -p ~/projects/cross-compilers

echo "Agregue la siguiente línea al final de su archivo ~/.bashrc"
echo "export PATH=$PATH:/opt/gcc-arm-compile/gcc-arm-none-eabi-6-2017-q2-update/bin/"
echo "Ejemplo:"
echo "echo \"export PATH=$PATH:/opt/gcc-arm-compile/gcc-arm-none-eabi-6-2017-q2-update/bin/\" > ~/.bashrc"
