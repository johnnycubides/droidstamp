# Instalación del toolchain para ChibiOS

## 1. Descargar el crosscompilador

```bash
wget -O "gcc-arm-none-eabi-6-2017-q2-update-linux.tar.bz2" https://developer.arm.com/-/media/Files/downloads/gnu-rm/6-2017q2/gcc-arm-none-eabi-6-2017-q2-update-linux.tar.bz2?revision=2cc92fb5-3e0e-402d-9197-bdfc8224d8a5?product=GNU%20Arm%20Embedded%20Toolchain,64-bit,,Linux,6-2017-q2-update
```

## 2. Instalar el crosscompilador

```bash
sudo tar xvf gcc-arm-none-eabi-6-2017-q2-update-linux.tar.bz2 -C /opt/gcc-arm-compile/
```
### 2.1. Agregar el directorio del crosscompilador al PATH general

Éste proceso puede ser resuelto de varias maneras, en éste **ejemplo** se agregará el PATH al **~/.bashrc**:

Abrir el archivo **~/.bashrc**
```bash
nano ~/.bashrc
```

Escribir al final del archivo lo siguiente y guardar:

```bash
export PATH=$PATH:/opt/gcc-arm-compile/gcc-arm-none-eabi-6-2017-q2-update/bin/
```

**nota**: guardar en *nano* se hace con el comando `ctrl+o` y después `ENTER`  y para salir `ctrl+x`.

Finalmente reinicie o lance otra terminal o cargue el bashrc en ésta terminal, ejemplo: `source ~/.bashrc`.

## 3. Descomprimir el Toolchain de ChibiOS y crear enlace simbólico

### 3.1 Descomprimir el Toolchain de ChibiOS
```bash
7z x ChibiOS_19.1.3.7z
```

### 3.2 Crear enlace simbólico

```bash
ln -s $PWD/ChibiOS_19.1.3 $HOME/.chibios
```

**Observación**: Si el enlace simbólico no se crea correctamente (se evidencia al momento
de compilar), remueva el enlace simbólico creado y crear el enlace simbólico como sigue:

```
rm ~/.chibios
ln -s /home/<usuario>/<path>/droidstamp/stm32f405/ChibiOS/ChibiOS_19.1.3 /home/<user>/.chibios
```

Donde `<usuario>` es su usario (lo pude detectar con el comando `whoami`) y el `<path>`
se refiere al path de instalación (clonación del repositorio) del droid stamp


## 4. Hacer el Hola Mundo

### 4.1 Croscompilar y enviar firmware al STM32

```bash
cd chibios-droidstamp-stm32f405
make
make s2st
```

**Observación**: Si está usando otra **ip** para la droipstamp, puede hacer uso del comando agregando `IP=nuevaIP`, ejemplo:

```bash
make s2st IP=192.168.1.101
```
**Observación**: La contraseña del droidstamp por default es `piccolo`.

### 4.2 Programar el STM32

Entrar al sistema del embebido (droidstamp) a través de la ip por ssh, ejemplo:

```bash
ssh root@192.168.15.1
```

```bash
cd /home/stm32
make flash
```
