# HTTP Server

https://blog.anvileight.com/posts/simple-python-http-server/

https://docs.python.org/3.3/library/http.server.html

https://www.acmesystems.it/python_http

https://gist.github.com/jfhbrook/1794968

https://docs.python.org/2/library/basehttpserver.html#BaseHTTPServer.BaseHTTPRequestHandler

## Ejemplos

### Server HTTP métodos GET

```py
#!/usr/bin/python
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer

PORT = 3000


class myHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        if self.path == "/helloword":
            self.cb_hellword()
        elif self.path == "/other":
            self.cb_other()

    def cb_hellword(self):
        self.send_response(200)
        self.end_headers()
        self.wfile.write(b'Hello Word')

    def cb_other(self):
        pass


try:
    # Create a web server and define the handler to manage the
    # incoming request
    server = HTTPServer(('', PORT), myHandler)
    print 'Started httpserver on port ', PORT
    # Wait forever for incoming htto requests
    server.serve_forever()

except KeyboardInterrupt:
    print '^C received, shutting down the web server'
    server.socket.close()
```

Puede cambiar el método `cb_hellword` por el de intés teniendo en cuenta el `path` asociado
