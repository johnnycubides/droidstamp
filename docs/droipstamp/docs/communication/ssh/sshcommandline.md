# Ejemplo de conexión a través de ssh

**Atención**: Debe estar conectado a través de WiFi a la droidstamp.

Abrir la terminal y ejecutar el siguiente comando seguido del password (por default es `piccolo`)

```bash
ssh root@192.168.15.1
```

Ejemplo:

<script id="asciicast-RWKltOB6WAX6Rdkm4yGeewx7o" src="https://asciinema.org/a/RWKltOB6WAX6Rdkm4yGeewx7o.js" async></script>
