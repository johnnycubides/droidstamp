# Comunicación con JuiceSSH

*Juice SSH* es una aplicación que se instala en el S.O. Android y es descargable
desde el *Play Store*

[Link juicessh android](https://juicessh-ssh-client.uptodown.com/android)

## Ejemplo de configuración

A continuación se muestra la vista principal de la aplicación.

![juice vista principal](main.png){: style="width:400px"}

Acceda a *Connections* y cree una nueva conexión con el botón **+**.

![juice conexiones](connections.png){: style="width:400px"}

Use el siguiente ejemplo para conectar la droidstamp:

* **ip**: 192.168.15.1
* **port**: 22

Agreguela al grupo y termine con el visto.

![Ejemplo de conexión para la droid](droidExample.png){: style="width:400px"}

Finalmente conecte al droidstamp

La aplicación le pedirá agregar la llave pública y poner la clave (la clave por default es `piccolo`)
