# Creación de particiones y formato para la microSD

Puede hacer uso de una `microSD` que tenga una capacidad de almacenamiento de **2GB** en adelante.

**Atención**: Para saber qué *script* usar, tenga encuenta lo siguiente:

* `./ext3_format_sd.sh`:  si hará uso de la combinación **imx23/kernel-2.6.35+rootfs** de éste repositorio

... Si se da soporte a otras combinaciones se agregarán a la anterior lista.

Johnny Cubides
