#!/bin/bash -e
#						 ↑
# debug [-x -v]:[complete, abbreviated]
# Brief:	brief
# Author: Johnny Cubides
# e-mail: jgcubidesc@gmail.com
# date: Monday 17 February 2020
status=$?

# foreground
BLACK=`tput setaf 0`
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
BLUE=`tput setaf 4`
MAGENTA=`tput setaf 5`
CYAN=`tput setaf 6`
WHITE=`tput setaf 7`
NC=`tput setaf 7`
# background
BLACKB=`tput setab 0`
REDB=`tput setab 1`
GREENB=`tput setab 2`
YELLOWB=`tput setab 3`
BLUEB=`tput setab 4`
MAGENTAB=`tput setab 5`
CYANB=`tput setab 6`
WHITEB=`tput setab 7`
NCB=`tput setab 0`

micro_sd=""
p1=""
p2=""

createParttions(){
  # sudo umount /media/`whoami`/*
  echo -e "\nd\nn\n\n\n\n+200M\nn\n\n\n\n\nt\n1\n53\nw" | sudo fdisk $micro_sd
  sudo mkfs.ext3 $micro_sd$p2
  sync
}

if [ -b /dev/sdb ]; then
  micro_sd=/dev/sdb
  p1=1
  p2=2
elif [ -b /dev/mmcblk0 ]; then
  micro_sd=/dev/mmcblk0
  p1=p1
  p2=p2
else
  print " ${RED}No existe una micro SD con la cual trabajar${NC}\n"
  exit 1
fi

read -p " ${YELLOW}Verifique que vaya a usar la tarjeta SD en cuestion $micro_sd
ya que después de éste proceso el contenido de la micro SD no será recuperable${NC}
desea continuar? y/n: " -r variable

if [ "$variable" = "y" ];
then
  createParttions
fi
